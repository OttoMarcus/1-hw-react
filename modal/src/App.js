import React from 'react';
import './App.css';
import RedModalComponent from "./components/Modal/redModal/RedModalGroup.jsx";
import YellowModalComponent from "./components/Modal/yellowModal/YellowModalGroup.jsx"

function App() {

    return (
        <>
            <div className="btn-group">
                <RedModalComponent />
                <YellowModalComponent />     
            </div>
           
        </>
    );
}

export default App;
