import React from 'react';
import './Button.scss';

function Button ({ borderColor, backgroundColor, text, onClick, color }) {
    return (
        <button
            style={{borderColor, backgroundColor, color }}
            className="btn"
            onClick={onClick}
        >
            {text}
        </button>
    );
};

export default Button;
