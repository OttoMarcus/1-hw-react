import React, { useState } from 'react';
import Button from '../../Buttons/Button';
import Modal from '../Modal';

function RedModalComponent() {
  const [redModal, setRedModal] = useState(false);

  return (
    <>
      <Button
        color="white"
        borderColor="#990000"
        backgroundColor="#FF0000"
        text="Open first modal"
        onClick={() => setRedModal(true)}
      />
      {redModal && (
        <Modal
          header="Do you want to delete this file?"
          modalClassName={"modal-red"}
          modalHeaderExtra="modal-header-red"
          btnFooterExtra="btn-footer-red"
          closeButton={true}
          text={
            <>
              Once you delete this file, it won't be possible to undo this action.
              <br />
              <div style={{ textAlign: 'center' }}>Are you sure you want to delete it?</div>
            </>
          }
          onClose={() => setRedModal(false)}
        />
      )}
    </>
  );
}

export default RedModalComponent