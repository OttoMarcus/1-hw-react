import React from 'react';
import './Modal.scss';
import "./redModal/redStyles.scss";
import "./yellowModal/yellowStyless.scss";

function Modal({ header,  modalClassName, closeButton, text, actions, onClose, modalHeaderExtra, btnFooterExtra }) {
    const modalHeader = "modalHead" + (modalHeaderExtra ? " " + modalHeaderExtra : "")
    const btnFooter = "btn-footer" + (btnFooterExtra ?" " + btnFooterExtra : "")
    

    return (
        <div className="backdrop" onClick={onClose}>
            <div className={modalClassName} onClick={(e) => e.stopPropagation()}>
                <div className={modalHeader}>
                    <h4>{header}</h4>
                    {closeButton && <button className="close-button" onClick={onClose}>X</button>}
                </div>
                <div className="modalBody">
                    <p>{text}</p>

                </div>
                <div className="modalFooter">
                    <button type="button" className={btnFooter} >Ok</button>
                    <button type="button" className={btnFooter} onClick={onClose}>Cancel</button>
                </div>
                <div className="modal-actions">{actions}</div>
            </div>
        </div>
    );
}

export default Modal;

