import React, { useState } from 'react';
import Button from '../../Buttons/Button';
import Modal from '../Modal';

function YellowModalComponent() {
    const [yellowModal, setYellowModal] = useState(false);
  
    return (
      <>
        <Button
          color="black"
          borderColor="#DAA520"
          backgroundColor="wheat"
          text="Open second modal"
          onClick={() => setYellowModal(true)}
        />
        {yellowModal && (
          <Modal
            header="Do you want to download the file?"
            modalClassName={"modal-yellow"}
            modalHeaderExtra="modal-header-yellow"
            btnFooterExtra="btn-footer-yellow"
            closeButton={true}
            text="If you click on the <OK> button, you agree with our terms."
            onClose={() => setYellowModal(false)}
          />
        )}
      </>
    );
  }

  export default YellowModalComponent